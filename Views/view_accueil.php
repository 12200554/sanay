<!-- Page d'accueil -->
<?php
require 'view_begin.php';
require 'view_header.php';
?>

<div class='main-container'>
    <div class="dashboard-container">
    <h1>Bienvenue sur l'extranet</h1>
        <div class='dashboard__table'>
            <h2 style='text-align:center;'>Bienvenue sur l'extranet dynamique de Perform Vision</h2>
            </br> 
            <h2> Révolutionnez votre gestion des bons de livraison grâce à notre solution moderne, intuitive et performante, conçue pour optimiser vos opérations et simplifier vos processus logistiques.</h2>
        </div>    
    </div>
</div>

<?php
require 'view_end.php';
?>
