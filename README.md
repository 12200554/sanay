# Projet SAÉ S401

## Équipe

- **Nom de l'équipe :** SANAY
- **Membres de l'équipe :**
  - Sophie Dong
  - Arnaud Aublet
  - Nassim Manseur
  - Antoine Da Costa
  - Younes Benaissa

## But du Projet

### SAÉ S401

L'objectif de ce projet est d'optimiser, en équipe, une application existante en suivant une démarche itérative ou incrémentale. En reprenant une application existante, nous visons à l'améliorer en fonction des paradigmes de qualité tels que l'ergonomie, la qualité logicielle, et en mettant particulièrement l'accent sur :

- **Accessibilité :** Rendre l'application utilisable par le plus grand nombre de personnes, y compris celles ayant des handicaps.
- **Impact Environnemental :** Réduire la consommation de ressources et optimiser l'efficacité énergétique.
- **Sécurité :** Assurer la protection des données utilisateurs et la résistance aux attaques.

Nous nous engageons à améliorer l'application de manière continue, en intégrant les retours des utilisateurs et en adoptant les meilleures pratiques de développement.

## Structure 

- le répertoire `Content` : où se trouvent la feuille de style CSS et les images utilisées.
 - le répertoire `Controllers` : contenant les contrôleurs qui gèrent les interactions entre les modèles et les vues.
- le répertoire `Models` : Ce répertoire contient les modèles de l'application.
- le répertoire `Utils` : Ce répertoire contient des fonctions réutilisables dans tout le projet.
- le répertoire `Views`: où sont stockées les vues, qui sont responsables de l'affichage des données et de l'interface utilisateur.
- le répertoire `html`: Ce répertoire contient la documentation HTML Doxygen.
- le script SQL `Script_SANAY.sql` : où se trouve le script de la base de données utilisé.

---

Site accessible depuis le lien : https://jupyter.univ-paris13.fr/~12200554/sanay/

---

Pour toute question ou suggestion, n'hésitez pas à nous contacter.
